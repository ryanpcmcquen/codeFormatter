# codeFormatter

A library to morph back-tick wrapped markup in to `<code>`.

https://github.com/ryanpcmcquen/codeFormatter

There is code in here to support the PrettyPrint library for syntax highlighting, but unfortunately, all the times I have submitted code that included PrettyPrint to various browser extension repositories, it has been an uphill battle that has many times prevented extensions from being accepted.

For that reason, I've removed all of the PrettyPrint embedded code from here. If you care to use this in your own work, merely including PrettyPrint and running it after `codeFormatter` invokes will be enough to take advantage of syntax highlighting.
